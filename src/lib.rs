#[cfg(test)]
extern crate rand;

use std::ops::{Neg, AddAssign, Add, SubAssign, Sub, Mul, MulAssign, Div, DivAssign};
use std::fmt::{Debug, Formatter, Error};

include!(concat!(env!("OUT_DIR"), "/to_float_lookup.rs"));
include!(concat!(env!("OUT_DIR"), "/elut_table.rs"));

#[repr(C)]
union HalfValue {
    i: u32,
    f: f32,
}

/// The 16 bit floating point number (half-precision floating-point).
/// Type half can represent positive and negative numbers whose
/// magnitude is between roughly 6.1e-5 and 6.5e+4 with a relative
/// error of 9.8e-4; numbers smaller than 6.1e-5 can be represented
/// with an absolute error of 6.0e-8.  All integers from -2048 to
/// +2048 can be represented exactly.
/// 
/// Conversions from half to float are lossless; all half numbers
/// are exactly representable as floats.
///
/// Conversions from float to half may not preserve a float's value
/// exactly.  If a float is not representable as a half, then the
/// float value is rounded to the nearest representable half.  If a
/// float value is exactly in the middle between the two closest
/// representable half values, then the float value is rounded to
/// the closest half whose least significant bit is zero.
/// 
/// Overflows during float-to-half conversions cause arithmetic
/// exceptions.  An overflow occurs when the float value to be
/// converted is too large to be represented as a half, or if the
/// float value is an infinity or a NAN.
pub struct Half {
    value: HalfValue,
    _h: u16,
}

impl Clone for Half {
    fn clone(&self) -> Self {
        Half {
            value: HalfValue { i: unsafe { self.value.i } as u32 },
            _h: self._h
        }
    }
}

impl Half {
    pub fn min_exponent() -> i32 {
        -13
    }

    pub fn max_exponent() -> i32 {
        16
    }

    pub fn min_exponent_10() -> i32 {
        -4
    }

    pub fn max_exponent_10() -> i32 {
        4
    }

    pub fn epsilon() -> f32 {
        0.00097656f32
    }

    pub fn min() -> f32 {
        5.96046448e-08f32
    }

    pub fn min_normalized() -> f32 {
        6.10351562e-05f32
    }

    pub fn max() -> f32 {
        65504f32
    }

    pub fn new() -> Half {
        Half {
            value: HalfValue { i: 0 },
            _h: 0
        }
    }

    fn convert(&self, value: i32) -> u16 {
        let s = (value >> 16) & 0x00008000;
        let mut e = ((value >> 23) & 0x000000ff) - (127 - 15);
        let mut m = value & 0x007fffff;

        if e <= 0 {
            if e < -10 {
                return s as u16;
            }

            m = m | 0x00800000;

            let t = 14 - e;
            let a = (1 << (t - 1)) - 1;
            let b = (m >> t) & 1;
            
            m = (m + a + b) >> t;

            return (s | m) as u16;
        } else if e == 0xff - (127 - 15) {
            if m == 0 {
                return (s | 0x7c00) as u16;
            } else {
                m = m >> 13;
                let significand = if m == 0 { 1 } else { 0 };
                return (s | 0x7c00 | m | significand) as u16;
            }
        } else {
            m = m + 0x00000fff + ((m >> 13) & 1);
            if (m & 0x00800000) != 0 {
                m = 0;
                e = e + 1;
            }
            if e > 30 {
                return (s | 0x7c00) as u16;
            }

            return (s | (e << 10) | (m >> 13)) as u16;
        }
    }

    /// Round to n-bit precision (n should be between 0 and 10).
    /// After rounding, the significand's 10-n least significant
    /// bits will be zero.
    pub fn round(&self, precision: u32) -> Half {
        if precision >= 10 {
            return self.clone();
        }

        let s = self._h & 0x8000;
        let mut e = self._h & 0x7fff;

        e = e >> (9 - precision);
        e = e + (e & 1);
        e = e << (9 - precision);

        if e >= 0x7c00 {
            e = self._h;
            e = e >> (10 - precision);
            e = e << (10 - precision);
        }

        let mut h = Half::new();
        h._h = s | e;
        h        
    }

    pub fn as_f32(&self) -> f32 {
        (unsafe { (HalfValue { i: FLOAT_LOOKUP[self._h as usize] }).f }) as f32
    }

    pub fn is_finite(&self) -> bool {
        let e = (self._h >> 10) & 0x001f;
        e < 31
    }

    pub fn is_normalized(&self) -> bool {
        let e = (self._h >> 10) & 0x001f;
        e > 0 && e < 31
    }

    pub fn is_denormalized(&self) -> bool {
        let e = (self._h >> 10) & 0x001f;
        let m = self._h & 0x3ff;
        e == 0 && m != 0        
    }

    pub fn is_zero(&self) -> bool {
        (self._h & 0x7fff) == 0
    }

    pub fn is_nan(&self) -> bool {
        let e = (self._h >> 10) & 0x001f;
        let m = self._h & 0x3ff;
        e == 31 && m != 0
    }

    pub fn is_infinity(&self) -> bool {
        let e = (self._h >> 10) & 0x001f;
        let m = self._h & 0x3ff;
        e == 31 && m == 0
    }

    pub fn is_negative(&self) -> bool {
        (self._h & 0x8000) != 0
    }

    pub fn positive_infinity() -> Half {
        let mut half = Half::new();
        half._h = 0x7c00;
        half
    }

    pub fn negative_infinity() -> Half {
        let mut half = Half::new();
        half._h = 0xfc00;
        half
    }

    pub fn q_nan() -> Half {
        let mut half = Half::new();
        half._h = 0x7fff;
        half
    }

    pub fn s_nan() -> Half {
        let mut half = Half::new();
        half._h = 0x7dff;
        half
    }

    /// Get the half bits
    pub fn bits(&self) -> u16 {
        self._h as u16
    }

    /// Set the half bits
    pub fn set_bits(&mut self, bits: u16) {
        self._h = bits as u16;
    }
}

impl From<f32> for Half {
    fn from(value: f32) -> Self {
        let mut half = Half {
            value: HalfValue { f: value },
            _h: 0,
        };
        if value == 0f32 {
            half._h = ((unsafe { half.value.i } as i32) >> 16) as u16;
        } else {
            let mut e: u16 = (((unsafe { half.value.i } as u32) >> 23) & 0x000001ff) as u16;
            e = ELUT_TABLE[e as usize];
            if e != 0 {
                let m = (unsafe { half.value.i } as i32) & 0x007fffff;
                half._h = ((e as i32) + ((m + 0x00000fff + ((m >> 13) & 1)) >> 13)) as u16;
            } else {
                half._h = half.convert(unsafe { half.value.i } as i32);
            }
        }
        half
    }
}

impl From<Half> for f32 {
    fn from(value: Half) -> Self {
        value.as_f32()
    }
}

impl Neg for Half {
    type Output = Self;

    fn neg(self) -> Half {
        let mut result = Half::new();
        result._h = self._h ^ 0x8000;
        result
    }
}

impl AddAssign for Half {
    fn add_assign(&mut self, other: Self) {
        *self = Half::from(self.as_f32() + other.as_f32());
    }
}

impl Add<Half> for Half {
    type Output = Half;

    fn add(self, other: Self) -> Self {
        Half::from(self.as_f32() + other.as_f32())
    }
}

impl Add<f32> for Half {
    type Output = Half;

    fn add(self, other: f32) -> Self {
        Half::from(self.as_f32() + other)
    }
}

impl SubAssign for Half {
    fn sub_assign(&mut self, other: Self) {
        *self = Half::from(self.as_f32() - other.as_f32());
    }
}

impl Sub<Half> for Half {
    type Output = Half;

    fn sub(self, other: Self) -> Self {
        Half::from(self.as_f32() - other.as_f32())
    }
}

impl Sub<f32> for Half {
    type Output = Half;

    fn sub(self, other: f32) -> Self {
        Half::from(self.as_f32() - other)
    }
}

impl MulAssign for Half {
    fn mul_assign(&mut self, other: Self) {
        *self = Half::from(self.as_f32() * other.as_f32());
    }
}

impl Mul<Half> for Half {
    type Output = Half;

    fn mul(self, other: Self) -> Self {
        Half::from(self.as_f32() * other.as_f32())
    }
}

impl Mul<f32> for Half {
    type Output = Half;

    fn mul(self, other: f32) -> Self {
        Half::from(self.as_f32() * other)
    }
}

impl DivAssign for Half {
    fn div_assign(&mut self, other: Self) {
        *self = Half::from(self.as_f32() / other.as_f32());
    }
}

impl Div<Half> for Half {
    type Output = Half;

    fn div(self, other: Self) -> Self {
        Half::from(self.as_f32() / other.as_f32())
    }
}

impl Div<f32> for Half {
    type Output = Half;

    fn div(self, other: f32) -> Self {
        Half::from(self.as_f32() / other)
    }
}

impl Debug for Half {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "Half {{ _h: {} }}", self._h)
    }
}

impl PartialEq for Half {
    fn eq(&self, other: &Self) -> bool {
        self._h == other._h
    }
}

impl Eq for Half {}

#[cfg(test)]
mod arithmetic_tests {
    use super::*;

    #[test]
    pub fn conversion_for_zero() {
        let expected = 0.0;
        let half = Half::from(expected);
        assert_eq!(expected, half.into());
    }

    #[test]
    pub fn conversion_for_point_five_works() {
        let expected = 0.5;
        let half = Half::from(expected);
        assert_eq!(expected, half.into());
    }

    #[test]
    pub fn conversion_for_hundred() {
        let expected = 100.0;
        let half = Half::from(expected);
        assert_eq!(expected, half.into());
    }

    #[test]
    pub fn conversion_for_infinite() {
        let expected = 1000000000.0;
        let half = Half::from(expected);
        let f: f32 = half.into();
        assert!(f.is_infinite());
    }

    #[test]
    pub fn rounding() {
        let half = Half::from(10.015);
        let rounded = half.round(2);
        assert_eq!(10f32, rounded.into());
        
        let half = Half::from(0.05);
        let rounded = half.round(2);
        assert_eq!(0.0468750000f32, rounded.into());
    }

    #[test]
    pub fn negation() {
        let half = Half::from(11f32);
        let negated = -half;
        assert_eq!(-11f32, negated.into());
    }

    #[test]
    pub fn add_assign() {
        let mut half = Half::from(4f32);
        half += Half::from(1f32);
        assert_eq!(5f32, half.into());
    }

    #[test]
    pub fn add() {
        let half = Half::from(4f32);
        let result = half + Half::from(1f32);
        assert_eq!(5f32, result.as_f32());
    }

    #[test]
    pub fn add_with_f32() {
        let half = Half::from(4f32);
        let result = half + 1f32;
        assert_eq!(5f32, result.as_f32());
    }

    #[test]
    pub fn subtract_assign() {
        let mut half = Half::from(4f32);
        half -= Half::from(1f32);
        assert_eq!(3f32, half.into());
    }

    #[test]
    pub fn subtract() {
        let half = Half::from(4f32);
        let result = half - Half::from(1f32);
        assert_eq!(3f32, result.as_f32());
    }

    #[test]
    pub fn subtract_with_f32() {
        let half = Half::from(4f32);
        let result = half - 1f32;
        assert_eq!(3f32, result.as_f32());
    }

    #[test]
    pub fn multiplication_assign() {
        let mut half = Half::from(4f32);
        half *= Half::from(2f32);
        assert_eq!(8f32, half.into());
    }

    #[test]
    pub fn multiplication() {
        let half = Half::from(4f32);
        let result = half * Half::from(2f32);
        assert_eq!(8f32, result.as_f32());
    }

    #[test]
    pub fn multiplication_with_f32() {
        let half = Half::from(4f32);
        let result = half * 2f32;
        assert_eq!(8f32, result.as_f32());
    }

    #[test]
    pub fn division_assign() {
        let mut half = Half::from(4f32);
        half /= Half::from(2f32);
        assert_eq!(2f32, half.into());
    }

    #[test]
    pub fn division() {
        let half = Half::from(4f32);
        let result = half / Half::from(2f32);
        assert_eq!(2f32, result.as_f32());
    }

    #[test]
    pub fn division_with_f32() {
        let half = Half::from(4f32);
        let result = half / 2f32;
        assert_eq!(2f32, result.as_f32());
    }
}

#[cfg(test)]
mod limits_test {
    use super::*;

    fn mypow(x: i32, y: i32) -> f32 {
        let x = x;
        let mut y = y;
        let mut negative = false;
        if y < 0 {
            negative = true;
            y = -y;
        }

        let mut z = 1f32;
        while y > 0 {
            z *= x as f32;
            y -= 1;
        }

        if negative {
            z = 1f32 / z;
        }

        z
    }

    #[test]
    pub fn min_exponent_normalized() {
        let half = Half::from(mypow(2, Half::min_exponent() - 1));
        assert!(half.is_normalized());
    }

    #[test]
    pub fn min_exponent_is_denormalized() {
        let half = Half::from(mypow(2, Half::min_exponent() - 2));
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn max_exponent_normalized() {
        let half = Half::from(mypow(2, Half::max_exponent() - 1));
        assert!(half.is_normalized());
    }

    #[test]
    pub fn max_exponent_infinity() {
        let half = Half::from(mypow(2, Half::max_exponent()));
        assert!(half.is_infinity());
    }

    #[test]
    pub fn min_exponent_10_normalized() {
        let half = Half::from(mypow(10, Half::min_exponent_10()));
        assert!(half.is_normalized());
    }

    #[test]
    pub fn min_exponent_10_denormalized() {
        let half = Half::from(mypow(10, Half::min_exponent_10() - 1));
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn max_exponent_10_normalized() {
        let half = Half::from(mypow(10, Half::max_exponent_10()));
        assert!(half.is_normalized());
    }

    #[test]
    pub fn max_exponent_10_infinity() {
        let half = Half::from(mypow(10, Half::max_exponent_10() + 1));
        assert!(half.is_infinity());
    }
}

#[cfg(test)]
mod classification_test {
    use super::*;

    #[test]
    pub fn zero_is_zero() {
        let half = Half::from(0f32);
        assert!(half.is_zero());
    }

    #[test]
    pub fn one_is_finite_and_normalized() {
        let half = Half::from(1f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn one_plus_epsilon_is_finite_and_normalized() {
        let half = Half::from(1f32 + Half::epsilon());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn min_is_finite_and_denormalized() {
        let half = Half::from(Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn min_times_two_is_finite_and_denormalized() {
        let half = Half::from(Half::min() + Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn min_normalized_is_finite_and_normalized() {
        let half = Half::from(Half::min_normalized());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn min_normalized_plus_min_is_finite_and_normalized() {
        let half = Half::from(Half::min_normalized() + Half::min());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn min_normalized_minus_min_is_finite_and_denormalized() {
        let half = Half::from(Half::min_normalized() - Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }
    
    #[test]
    pub fn two_is_finite_and_normalized() {
        let half = Half::from(2f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn three_is_finite_and_normalized() {
        let half = Half::from(3f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn point_one_is_finite_and_normalized() {
        let half = Half::from(0.1f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn point_two_is_finite_and_normalized() {
        let half = Half::from(0.2f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn point_three_is_finite_and_normalized() {
        let half = Half::from(0.3f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn max_is_finite_and_normalized() {
        let half = Half::from(Half::max());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn positive_infinity_is_infinity() {
        let value = HalfValue { i: 0x7f800000 };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_infinity());

        let half = Half::positive_infinity();
        assert!(half.is_infinity());
    }
    
    #[test]
    pub fn positive_q_nan_is_nan() {
        let value = HalfValue { i: 0x7fffffff };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_nan());

        let value = HalfValue { i: 0x7fd55555 };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_nan());

        let half = Half::q_nan();
        assert!(half.is_nan());
    }

    #[test]
    pub fn negative_one_is_finite_and_normalized() {
        let half = Half::from(-1f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn negative_one_min_epsilon_is_finite_and_normalized() {
        let half = Half::from(-1f32 - Half::epsilon());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn negative_min_is_finite_and_denormalized() {
        let half = Half::from(-Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn negative_min_minus_min_is_finite_and_denormalized() {
        let half = Half::from(-Half::min() - Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }

    #[test]
    pub fn negative_min_normalized_is_finite_and_normalized() {
        let half = Half::from(-Half::min_normalized());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn negative_min_normalized_minus_min_is_finite_and_normalized() {
        let half = Half::from(-Half::min_normalized() - Half::min());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn negative_min_normalized_plus_min_is_finite_and_denormalized() {
        let half = Half::from(-Half::min_normalized() + Half::min());
        assert!(half.is_finite());
        assert!(half.is_denormalized());
    }
    
    #[test]
    pub fn negative_two_is_finite_and_normalized() {
        let half = Half::from(-2f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }

    #[test]
    pub fn negative_three_is_finite_and_normalized() {
        let half = Half::from(-3f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn negative_point_one_is_finite_and_normalized() {
        let half = Half::from(-0.1f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn negative_point_two_is_finite_and_normalized() {
        let half = Half::from(-0.2f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn negative_point_three_is_finite_and_normalized() {
        let half = Half::from(-0.3f32);
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn negative_max_is_finite_and_normalized() {
        let half = Half::from(-Half::max());
        assert!(half.is_finite());
        assert!(half.is_normalized());
    }
    
    #[test]
    pub fn negative_infinity_is_infinity() {
        let value = HalfValue { i: 0xff800000 };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_infinity());

        let half = Half::negative_infinity();
        assert!(half.is_infinity());
    }
    
    #[test]
    pub fn negative_q_nan_is_nan() {
        let value = HalfValue { i: 0xffffffff };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_nan());

        let value = HalfValue { i: 0xffd55555 };
        let half = Half::from((unsafe { value.f }) as f32);
        assert!(half.is_nan());
    }
    
    #[test]
    pub fn negative_s_nan_is_nan() {
        let half = Half::s_nan();
        assert!(half.is_nan());
    }
}

#[cfg(test)]
mod rounding_error_tests {
    use super::*;
    use self::rand::Rng;

    const RAND_MAX: i32 = 0x7fff;

    fn drand() -> f32 {
        let mut random = rand::thread_rng();
        random.gen_range(0f32, RAND_MAX as f32) / (RAND_MAX as f32 + 1f32)
    }

    fn test_normalized_rounding(n: u32) -> bool {
        let e_expected = if n < 10 {
            Half::epsilon() * 0.5f32 * ((1 << (10 -n)) as f32)
        } else {
            0f32
        };

        for i in 0..200000 {
            let random = drand();
            let value = random * Half::max();
            let mut h = Half::from(value);
            if h.as_f32() < Half::min_normalized() {
                continue;
            }

            if (i & 1) != 0 {
                h = -h;
            }

            let h2 = Half::from(h.clone());
            let r = h.round(n);
            let e = 1f32 - (r.as_f32() / h2.as_f32());
            if e > e_expected {
                println!("Error: {}. Expected error: {}", e, e_expected);
                return false;
            }
        }

        true
    }

    fn test_denormalized_rounding(n: u32) -> bool {
        let e_expected = if n < 10 {
            Half::min() * 0.5f32 * ((1 << (10 -n)) as f32)
        } else {
            0f32
        };

        for i in 0..200000 {
            let random = drand();
            let value = random * (Half::min_normalized() - Half::min());
            let mut h = Half::from(value);

            if (i & 1) != 0 {
                h = -h;
            }

            let h2 = Half::from(h.clone());
            let r = h.round(n);
            let mut e = r.as_f32() - h2.as_f32();
            if e < 0f32 {
                e = -e;
            }

            if e > e_expected {
                println!("Error: {}. Expected error: {}", e, e_expected);
                return false;
            }
        }

        true
    }

    #[test]
    pub fn normalized_rounding_10_bit_precision()  {
        assert!(test_normalized_rounding(10));
    }

    #[test]
    pub fn normalized_rounding_9_bit_precision()  {
        assert!(test_normalized_rounding(10));
    }

    #[test]
    pub fn normalized_rounding_1_bit_precision()  {
        assert!(test_normalized_rounding(1));
    }

    #[test]
    pub fn normalized_rounding_0_bit_precision()  {
        assert!(test_normalized_rounding(0));
    }

    #[test]
    pub fn denormalized_rounding_10_bit_precision()  {
        assert!(test_denormalized_rounding(10));
    }

    #[test]
    pub fn denormalized_rounding_9_bit_precision()  {
        assert!(test_denormalized_rounding(10));
    }

    #[test]
    pub fn denormalized_rounding_1_bit_precision()  {
        assert!(test_denormalized_rounding(1));
    }

    #[test]
    pub fn denormalized_rounding_0_bit_precision()  {
        assert!(test_denormalized_rounding(0));
    }
}

#[cfg(test)]
mod conversion_error_tests {
    use super::*;
    use self::rand::Rng;

    const RAND_MAX: i32 = 0x7fff;

    fn drand() -> f32 {
        let mut random = rand::thread_rng();
        random.gen_range(0f32, RAND_MAX as f32) / (RAND_MAX as f32 + 1f32)
    }

    #[test]
    pub fn normalized_conversion() {
        for i in 0..20000000 {
            let mut f = drand() * Half::max();
            if f < Half::min_normalized() {
                continue;
            }

            if i & 1 != 0 {
                f = -f;
            }

            let h = Half::from(f);
            let mut e = 1f32 - (h.as_f32() / f);
            if e < 0f32 {
                e = -e;
            }

            if e > Half::epsilon() * 0.5f32 {
                println!("Float: {}, Half: {:?}, error: {}", f, h, e);
                assert!(false);
            }
        }
    }

    #[test]
    pub fn denormalized_conversion() {
        for i in 0..20000000 {
            let mut f = drand() * (Half::min_normalized() - Half::min());

            if i & 1 != 0 {
                f = -f;
            }


            let h = Half::from(f);
            let mut e = h.as_f32() - f;
            if e < 0f32 {
                e = -e;
            }

            if e > Half::min() * 0.5f32 {
                println!("Float: {}, Half: {:?}, error: {}", f, h, e);
                assert!(false);
            }
        }
    }

    #[test]
    pub fn temp() {
        let h = Half::from(0f32);
        assert_eq!(h.as_f32(), 0f32);
    }
}