#![feature(test)]

extern crate test;
extern crate halfpoint_rs;

use halfpoint_rs::Half;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn half_from_f32(b: &mut Bencher) {
        b.iter(|| Half::from(1f32));
    }

    #[bench]
    fn half_to_f32(b: &mut Bencher) {
        let half = Half::from(100f32);
        b.iter(|| half.as_f32());
    }

    #[bench]
    fn half_round(b: &mut Bencher) {
        let half = Half::from(10.2f32);
        b.iter(|| half.round(5));
    }
}